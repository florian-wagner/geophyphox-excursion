{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Modeling the gravimetric response of a subsurface anomaly in 2D"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook, we will simulate the gravimetric response (i.e., change in gravitational acceleration) due for a given subsurface density contrast. We start by importing the necessary packages:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Standard\n",
    "%matplotlib widget\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "plt.style.use(\"seaborn-notebook\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "## An analytical model for a horizontal cylinder\n",
    "\n",
    "A geophysical forward model could be represented by an analytical solution. If the geometry of the subsurface or the physical process of interest gets more complex, an analytical model might not suffice and one would need a numerical solution. But let's start with an analytical model first. In Geophysics I, we worked with the analytical solution for a buried horizontal cylinder.\n",
    "\n",
    "The analytical solution for a horizontal cylinder is given by:\n",
    "    \n",
    "\\begin{gather}\n",
    "    \\Delta g_z = \\frac{2 \\pi r^2 h \\Delta \\rho G}{x^2 + h^2}\n",
    "\\end{gather}\n",
    "\n",
    "where $g_z$ is the vertical component of the gravimetric anomaly in m/s$^2$, $G$ is the gravitational constant (6.6743 ×10$^{−11}$ m$^3$/(kg × s$^2$)), $r$ is the radius of the tunnel in meters, $\\Delta \\rho$ is the density contrast between the anomaly and the surrounding rock in kg/m$^3$, $h$ is the depth of the center of the tunnel, and $x$ is the location along the profile. Both $x$ and $h$ are in meters.\n",
    "\n",
    "<img src=\"http://fwagner.info/lehre/Grav_Anomaly.png\" alt=\"Drawing\" style=\"width: 50%;\">\n",
    "\n",
    "Let's first write a function which calculates the analytical solution for a specified cylindrical anomaly (in mGal).\n",
    "    \n",
    "**(Note: Pay attention to the units!)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def grav_cylinder(x, r, h, dRho):\n",
    "    \"\"\"Gravitational anomaly of a horizontal cylinder.\"\"\"\n",
    "    G = 6.6743e-11\n",
    "    gz = 2 * np.pi * r**2 * dRho * G * h / (x**2 + h**2)\n",
    "    gz *= 1e5 # unit conversion\n",
    "    return gz"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Settings\n",
    "r = 10       # radius\n",
    "dRho = 3000 # tunnel = decrease in density\n",
    "h = 15       # depth\n",
    "\n",
    "# Measurement locations along the profile (anomaly is always in the center)\n",
    "x = np.linspace(-50,50,51)\n",
    "\n",
    "analytical = grav_cylinder(x, r, h, dRho)\n",
    "plt.plot(x, analytical)\n",
    "plt.ylabel(\"$\\Delta g_z$ in mGal\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Let's add some interactivety"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ipywidgets import interact"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xobs = x\n",
    "#data = pd.read_csv(\"example_data/gravimetric_synthetic_data.csv\");\n",
    "#data = pd.read_csv(\"example_data/gravity_profile_duerwiss.csv\"); data[\"x (m)\"] -= data[\"x (m)\"].min(); xobs = data[\"x (m)\"]\n",
    "\n",
    "#gz_mes = data[\"g_z (mGal)\"]\n",
    "\n",
    "#data.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, (ax1, ax2) = plt.subplots(2,1)\n",
    "\n",
    "def plot_cylinder(xpos, radius, depth, dRho):\n",
    "    for a in (ax1, ax2):\n",
    "        a.clear()\n",
    "    xnum = np.linspace(xobs.min(), xobs.max(), 100) - xpos\n",
    "    dg = grav_cylinder(xnum, radius, depth, dRho)\n",
    "    #ax1.plot(xobs, gz_mes, 'ro', label=\"Measured\")\n",
    "    line, = ax1.plot(xnum + xpos, dg, 'b-', label=\"Simulated\")\n",
    "    circle = plt.Circle((xpos, -depth), radius, alpha=np.abs(dRho*0.0001), color=line.get_color())\n",
    "    ax1.set_ylim(0,1)\n",
    "    ax2.add_artist(circle)\n",
    "    ax2.set_ylim(-30, 0)\n",
    "    ax2.set_xlim(xobs.min(), xobs.max())\n",
    "    ax2.set_aspect(\"equal\")\n",
    "    for ax in ax1, ax2:\n",
    "        ax.set(adjustable='datalim')\n",
    "    fig.legend()\n",
    "    \n",
    "cylinder = interact(plot_cylinder,xpos=(xobs.min(),xobs.max(),1),radius=(0,10.0,0.5), depth=(1,30,1), dRho=(1,10000,500), continuous_update=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Numerical model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the circular anomaly is too simple to describe our measured data, we might want to move to a more complex geometry, which can only be handled by a numerical model. The modeling routine we will be using later allows to simulate the response for abitrary 2D polygons. We can use some of the helper functions in pyGIMLi to create these, e.g.:\n",
    "\n",
    "- [`pygimli.meshtools.createRectangle`](http://pygimli.org/pygimliapi/_generated/pygimli.meshtools.html#createrectangle)\n",
    "- [`pygimli.meshtools.createCircle`](http://pygimli.org/pygimliapi/_generated/pygimli.meshtools.html#createcircle)\n",
    "- [`pygimli.meshtools.createPolygon`](http://pygimli.org/pygimliapi/_generated/pygimli.meshtools.html#createpolygon) # arbitrary shapes based on a list of points"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pygimli as pg\n",
    "from pygimli.meshtools import createRectangle, createCircle, createPolygon"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The modeling routine we will use is based on *Won & Bevis (1987)* and is described in more detail in [this paper](https://www.u-cursos.cl/ingenieria/2010/1/GF700/1/material_docente/bajar?id_material=280333). We can import it from the provided file `modinv.py` and look at its documentation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from modinv import solveGravimetry\n",
    "help(solveGravimetry)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is always a good idea to compare the numerical solution with a simple geometry to an existing analytical solution for validation. The horizontal cylinder could be compared to a circle in 2D."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 10 # Deviations from the analytical solutions will mainly depend on the number of segments to represent the circle\n",
    "circle = createCircle(pos=[0, -h], radius=r, nSegments=n) \n",
    "numerical = solveGravimetry(circle, dRho, x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Visualization\n",
    "def showModel(rect, x, ax=None):\n",
    "    if ax is None:\n",
    "        fig, ax = plt.subplots()\n",
    "    ax.plot(x, np.zeros_like(x), \"v\", markersize=15, color=\"tab:orange\")\n",
    "    pg.show(rect, fillRegion=False, ax=ax)\n",
    "    ax.set_ylim(rect.ymin() - 10, 0)\n",
    "    ax.set_ylabel(\"z (m)\")\n",
    "    ax.set_xlabel(\"x (m)\")\n",
    "    ax.set_aspect(\"equal\")\n",
    "\n",
    "fig, (ax1, ax2) = plt.subplots(2,1, sharex=True)\n",
    "ax1.plot(x, analytical, \"k-\", label=\"Analytical solution\", alpha=0.5)\n",
    "ax1.plot(x, numerical, \"go\", label=\"Numerical solution\")\n",
    "ax1.set_ylabel(\"$\\Delta g_z$ in mGal\")\n",
    "ax1.legend()\n",
    "showModel(circle, x, ax2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After validation of the numerical solution, we can move to more complex geometries:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Creating measurement locations\n",
    "x2 = np.linspace(0,100,20) # 20 measurements locations on a 100 m profile\n",
    "\n",
    "# Creating a rectangular subsurface anomaly\n",
    "rect = createRectangle(start=[30,-20], end=[60,-10]) # Creating a rectangle\n",
    "rect.rotate([0, 0, np.deg2rad(-10)]) # Rotating by 10° counter-clockwise\n",
    "rect.translate([0, 8]) # Move 8 meters closer to the surface\n",
    "  \n",
    "showModel(rect, x2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculating the gravitational anomaly for a given density contrast"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)\n",
    "\n",
    "for dRho2 in -5000, -3000, -1000:\n",
    "    dg = solveGravimetry(rect, dRho2, x2)\n",
    "    ax1.plot(x2, dg, label=r\"$\\Delta \\rho =$ %d $kg/m^3$\" % dRho)\n",
    "\n",
    "ax1.set_ylabel(\"$\\Delta g_z$ in mGal\")\n",
    "ax1.legend()\n",
    "showModel(rect, x2, ax2)\n",
    "\n",
    "# Figures can be saved in high resolution for your presentations later\n",
    "# fig.savefig(\"simulated_anomalies.png\", dpi=300, bbox_inches=\"tight\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Interactively create a polygon that fits the data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def poly(points, smooth=False):\n",
    "    p = points.reshape(len(points)//2,2)\n",
    "    circ = createPolygon(p, isClosed=True, interpolate=\"linear\", addNodes=10)\n",
    "    gz_p = solveGravimetry(circ, -dRho, xobs)\n",
    "    return circ, gz_p"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "number_of_points = 6\n",
    "smooth = True\n",
    "\n",
    "fig, (ax1, ax2) = plt.subplots(2,1, sharex=True, figsize=(10,10))\n",
    "ax1.plot(xobs, gz_mes, \"ro\", label=\"Measured\")\n",
    "ax1.set_ylabel(\"$\\Delta g_z$ in mGal\")\n",
    "ax2.set_ylim(-25, 0)\n",
    "for ax in ax1, ax2:\n",
    "    ax.set(adjustable='datalim')\n",
    "text=ax2.text(0,0, \"\", va=\"top\", ha=\"left\")\n",
    "\n",
    "xp = []\n",
    "yp = []\n",
    "def onclick(event):\n",
    "    xp.append(event.xdata)\n",
    "    yp.append(event.ydata)\n",
    "    \n",
    "    ax2.scatter(xp, yp, c=\"g\")\n",
    "    ax2.plot(xp, yp, \"g-\")\n",
    "    ax2.set_title('Click to insert %d more polygon point in a clock-wise manner' % (number_of_points - len(xp)))\n",
    "    if len(xp) == number_of_points:\n",
    "        XY = np.array(list(zip(xp, yp)))\n",
    "        p0 = XY.flatten()\n",
    "        final, response = poly(p0, smooth)    \n",
    "        line, = ax1.plot(xobs, response, \"g--\", label=\"Simulated\")\n",
    "    if len(xp) >= number_of_points:\n",
    "        ax2.plot([xp[-1], xp[0]], [yp[-1], yp[0]], \"g-\")\n",
    "        ax1.legend()\n",
    "        fig.canvas.mpl_disconnect(cid)\n",
    "        \n",
    "\n",
    "cid = fig.canvas.mpl_connect('button_press_event', onclick)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "# Bonus question: How well does your model describe the measured data?\n",
    "\n",
    "Try to find out by calculating the RMS error:\n",
    "$\\sqrt{\\frac{\\sum_{i=1}^{N}\\left(\\text { Predicted }_{i}-\\text { Actual }_{i}\\right)^{2}}{N}}$"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
