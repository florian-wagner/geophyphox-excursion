import numpy as np
from pygimli.physics.gravimetry import solveGravimetry as sg

# Wrapper to pygimli version with better docstring / pnts handling
def solveGravimetry(poly, dRho, pnts):
    """Return vertical gravitational acceleration in mGal for one anomaly `poly` for a given density contrast `dRho` at measurement locations `pnts`.

    Parameters
    ----------
    poly : pyGIMLi geometry (i.e., mesh without cells)
        2D piecewise linear complex.
        
    dDensity : float
        Density difference between the surrounding and the anomaly compared (e.g., negative for density increase) in SI units (i.e., kg/m^3)

    pnts : [x1, x1] or [[x_i, z_i]]
        List of measurement positions, either 1D array with x positions only or 2D array with x and y positions.
    """
    pnts = np.array(pnts)
    if len(pnts.shape) == 1:
        pnts_new = np.zeros((pnts.shape[0], 2))
        pnts_new[:, 0] = pnts
        pnts = pnts_new
    elif len(pnts.shape) > 2:
        pg.warn("Check dimensions of provided meausrement points.")
    result = sg(poly, dRho, pnts)
    return np.array(result)

def fitlimits_old(px = None,pz = None): 
    """Function to find appropriate inversion limits for polygon points."""
    lv = len(px)
    dz = np.zeros(lv)
    dz[:-1] = np.abs(pz[np.arange(lv - 1)] - pz[np.arange(1,lv)])
    dz[-1] = np.abs(pz[-1] - pz[0])

    dx = np.zeros(lv)
    dx[:-1] = np.abs(px[np.arange(lv - 1)] - px[np.arange(1,lv)])
    dx[-1] = np.abs(px[-1] - px[0])
    d = (dx ** 2 + dz ** 2) ** 0.5

    # M(i)=1 wenn der Abstand zum Pkt. (i+1) gr��er ist als zu (i-1):
    M = np.zeros(lv)
    for i in np.arange(lv - 1):
        if d[i + 1] < d[i]:
            M[i + 1] = 1
    
    if d[0] < d[-1]:
        M[0] = 1
    
    # Berechnung der Varianz der Polygonpunkte:
    i = np.where(M > 0)[0]
    i1 = np.where(M < 1)[0]
    xv = np.zeros(lv)
    zv = np.zeros(lv)
    xv[i] = 2 ** 0.5 * d[i] / 4
    zv[i] = 2 ** 0.5 * d[i] / 4

    if i1[0] == 1:
        xv[0] = 2 ** 0.5 * d[-1] / 4
        zv[0] = 2 ** 0.5 * d[-1] / 4
        xv[i1[1:-1]] = 2 ** 0.5 * d[i1[1:-1] - 1] / 4
        zv[i1[1:-1]] = 2 ** 0.5 * d[i1[1:-1] - 1] / 4
    else:
        xv[i1] = 2 ** 0.5 * d[i1 - 1] / 4
        zv[i1] = 2 ** 0.5 * d[i1 - 1] / 4
    
    xll = px - xv
    xul = px + xv
    zul = pz + zv
    zll = pz - zv
    for i in np.arange(lv):
        if zv[i] < pz[i]:
            zll[i] = 0
        else:
            zll[i] = pz[i] - zv[i]
    zul[zul > 0] = 0
    
    ll = np.array([xll,zll])
    ul = np.array([xul,zul])
    
    return ll.T.flatten(),ul.T.flatten()

def fitlimits(px = None,pz = None): 
    """Function to find appropriate inversion limits for polygon points."""
    lv = len(px)
    dz = np.zeros(lv)
    dz[:-1] = np.abs(pz[np.arange(lv - 1)] - pz[np.arange(1,lv)])
    dz[-1] = np.abs(pz[-1] - pz[0])

    dx = np.zeros(lv)
    dx[:-1] = np.abs(px[np.arange(lv - 1)] - px[np.arange(1,lv)])
    dx[-1] = np.abs(px[-1] - px[0])
    d = (dx ** 2 + dz ** 2) ** 0.5

    # M(i)=1 wenn der Abstand zum Pkt. (i+1) gr��er ist als zu (i-1):
    M = np.zeros(lv)
    for i in np.arange(lv - 1):
        if d[i + 1] > d[i]:
            M[i + 1] = 1
    
    if d[0] < d[-1]:
        M[0] = 1
    
    # Berechnung der Varianz der Polygonpunkte:
    i = np.where(M > 0)[0]
    i1 = np.where(M < 1)[0]
    xv = np.zeros(lv)
    zv = np.zeros(lv)
    xv[i] = 2 ** 0.5 * d[i] / 4
    zv[i] = 2 ** 0.5 * d[i] / 4

    if i1[0] == 1:
        xv[0] = 2 ** 0.5 * d[-1] / 4
        zv[0] = 2 ** 0.5 * d[-1] / 4
        xv[i1[1:-1]] = 2 ** 0.5 * d[i1[1:-1] - 1] / 4
        zv[i1[1:-1]] = 2 ** 0.5 * d[i1[1:-1] - 1] / 4
    else:
        xv[i1] = 2 ** 0.5 * d[i1 - 1] / 4
        zv[i1] = 2 ** 0.5 * d[i1 - 1] / 4
    
    xll = px - xv
    xul = px + xv
    zul = pz + zv
    zll = pz - zv
    for i in np.arange(lv):
        if zv[i] < pz[i]:
            zll[i] = 0
        else:
            zll[i] = pz[i] - zv[i]
    zul[zul > 0] = 0
    
    ll = np.array([xll,zll])
    ul = np.array([xul,zul])
    
    #return ll, ul
    return ll.T.flatten(),ul.T.flatten()